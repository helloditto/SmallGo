<?php
/**
 * User: XiaoFei Zhai
 * Date: 17/9/27
 * Time: 上午10:56
 */

namespace App\Http\Controllers;


use App\Common\TaoBao;
use App\Models\Ad;
use App\Models\Category;
use App\Models\GoodsGallery;
use App\Models\GoodsShare;
use App\Models\History;
use App\Models\RecommendGoods;
use App\Models\TagGoods;
use Carbon\Carbon;
use TbkItemInfoGetRequest;
use TopClient;

class GoodsController extends Controller
{

    public function detail($id){
        $goods                                  =   GoodsShare::where(['id'=>$id,'status'=>1])->first();
        $data                                   =   [];
        if($goods){

            $categoryInfo                       =   Category::info($goods->category_id);
            $crumb[]                            =   ['title'=>$categoryInfo['name'],'url'=>url('/category',['id'=>$categoryInfo['id']])];
            $crumb[]                            =   ['title'=>$goods['name']];
            $data['goods']                          =   $goods;
            $data['title']                          =   $goods->title ? $goods->title : $goods->name;
            $data['crumb']                          =   $crumb;
            $data['code']                           =   base64_encode($goods->click_url);
        }


//        //记录历史
//        $userId                                 =   session('login_user_id');
//        if($userId){
//            $history                            =   new History();
//            $history->user_id                   =   $userId;
//            $history->goods_id                  =   $id;
//            $history->save();
//        }
        return $this->view('goods.item',$data);
    }
    public function info($num_iid){
        $click_url                          =   request()->click_url;
        $coupon_start_time                  =   request()->coupon_start_time;
        $coupon_end_time                    =   request()->coupon_end_time;
        $coupon_amount                      =   request()->coupon_amount;
        $taobao                             =   new TaoBao();
        $goods                              =   $taobao->item($num_iid);
        $goodsShare                         =   new GoodsShare();
        if ($goods){

            $goodsShare->id                     =   0;
            $goodsShare->name                   =   $goods['title'];
            $goodsShare->title                  =   $goods['title'];
            $goodsShare->pictures               =   $goods['pictures'];
            $goodsShare->price                  =   $goods['price'];
            $goodsShare->click_url              =   $click_url;
            $goodsShare->coupon_start_time     =   $coupon_start_time;
            $goodsShare->coupon_end_time       =   $coupon_end_time;
            $goodsShare->coupon_amount         =   $coupon_amount;
            $goodsShare->coupon_price          =   $goods['price']-$coupon_amount;
            $goodsShare->tpwd                  =   $taobao->tpwd($goods['title'],$click_url);
            $goodsShare->original_id            =   $num_iid;

        }
        $data['title']                      =   $goods['title'];
        $data['goods']                      =   $goodsShare;
        $data['code']                       =   base64_encode($click_url);
        return $this->view('goods.item',$data);
    }

}