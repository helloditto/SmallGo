<?php
/**
 * User: XiaoFei Zhai
 * Date: 17/10/18
 * Time: 下午3:30
 */
return [
    'app_key'        =>  env('TAOBAO_APP_KEY',''),
    'app_secret'     =>  env('TAOBAO_APP_SECRET',''),
    'ad_zone_id'     =>  env('AD_ZONE_ID')
];