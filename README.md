# SmallGo
   SmallGo是一个开源的淘宝客系统，支持pc和手机浏览，支持微信，基于全球最流行php框架laravel开发，后台采用laravel-admin开发，提供更方便、更安全的WEB应用开发体验，采用了全新的架构设计和命名空间机制，以帮助想做淘宝客的朋友。突破了传统淘宝客程序对自动采集商品收费的模式，代码不加密，方便大家修改。

   采用laravel作为开发框架，项目后台依赖laravel-admin搭建，需要安装PHP 7.1+和Laravel 5.5，php包管理采用composer，如您不了解composer，请自行百度学习，以下内容默认您对larvavel和composer已经了解或熟悉，前端包管理采用npm，前端资源编译采用laravel-mix

   ##主要特色：
   ######优惠券 淘口令 后台更新商品信息 支持微信
1、安装 

    composer install
2、执行（该命令源于laravel-admin）
    
    php artisan vendor:publish --provider="Encore\Admin\AdminServiceProvider"
    
3、执行如下命令

    php artisan smallgo:install
    
演示站点(搭建中) http://www.nayiya.com (支持pc和手机浏览)  
交流社区(搭建中) http://www.361dream.com