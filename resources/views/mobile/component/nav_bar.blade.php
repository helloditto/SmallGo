<section style="width: 100%;background-color: white">
    <div class="footer">
        <ul>
            <li class="active">
                <a href="{{url('')}}">
                    <i class="caesar">U</i>
                    <p class="pingfang">优选</p>
                </a>
            </li>
            <li>
                <a href="{{url('channel',['id'=>11])}}">
                    <i class="caesar">9.9</i>
                    <p class="pingfang">9.9包邮</p>
                </a>
            </li>
            <li>
                <a href="{{url('channel',['id'=>12])}}">
                    <i class="caesar">T</i>
                    <p class="pingfang">特价</p>
                </a>
            </li>
        </ul>
    </div>
    <div id="go-top">
        <i class="iconfont icon-fanhuidingbu"></i>
    </div>
</section>

