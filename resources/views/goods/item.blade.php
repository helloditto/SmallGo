@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="g-bd">
            <div class="row">
                <!--面包屑-->
                @include('component.crumb')
                <div class="goods-detail">
                    <div class="goods-header">
                        <div class="goods-picture">
                            <div class="play">
                                <div class="big_pic">
                                    @if(count($goods->pictures) > 0)
                                        @foreach($goods->pictures as $picture)
                                            @if ($loop->first)
                                                <img src="{{get_image_url($picture)}}"/>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                                <div class="small_pic">
                                    <ul>
                                        @if(count($goods->pictures) > 0)
                                            @foreach($goods->pictures as $picture)
                                                @if ($loop->first)
                                                    <li>
                                                        <img src="{{get_image_url($picture)}}"/>
                                                    </li>
                                                @else
                                                    @if($loop->index <5)
                                                        <li>
                                                            <img src="{{get_image_url($picture)}}"/>
                                                        </li>
                                                    @endif

                                                @endif
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="goods-info">
                            <h2 class="goods-title">{{ $goods->name }}</h2>
                            <div class="goods-price">
                                @if($goods->coupon_status > 0)
                                    <div class="coupon-box">
                                        <s class="original-price">价格:￥{{$goods->price}}</s>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <i class="iconfont icon-rmb"></i>
                                        <span class="coupon-price">{{ $goods->coupon_price }}</span>
                                        <img style="margin-bottom: -3px" src="{{asset('images/coupon_price.png')}}">
                                    </div>
                                @else
                                    价格:
                                    <span class="rmb">￥</span>
                                    <span class="price">{{ $goods->price }}</span>
                                @endif

                            </div>
                            <div class="buy">
                                @if($goods->coupon_status > 0)
                                    <a class="btn btn-l btn-default" href="{{ $goods->coupon_click_url }}">领券购买</a>
                                @else
                                    <a class="btn btn-l btn-default" href="{{ $goods->click_url }}">去购买</a>
                                @endif

                            </div>

                        </div>
                    </div>
                    <div class="goods-body clear">
                        <div class="detail-tab">
                            <ul class="nav">
                                <li class="item active">
                                    <a href="javascript:;">
                                        <span>详情</span>
                                    </a>
                                </li>
                                <li class="item ">
                                    <a href="javascript:;">
                                    <span>
                                        <span>评价</span>
                                        <span class="num">
                                            <span>（</span>
                                            <span>0</span>
                                            <span>）</span>
                                        </span>
                                    </span>
                                    </a>
                                </li>
                                <li class="bg"></li>
                            </ul>
                        </div>
                        <div class="content">
                            {!! $goods->detail  !!}
                        </div>

                    </div>

                </div>
            </div>
        </div>

    </div>

@endsection